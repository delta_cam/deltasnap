//
//  Delta.swift
//  DeltaCam
//
//  Created by Mars Huang on 5/28/16.
//  Copyright © 2016 DeltaCam. All rights reserved.
//

import UIKit
//temp change to Deltas ***************** Change back to Delta
class Deltas {
    //MARK: Properties
    
    var DeltaImages: [UIImage] = []
    var DeltaName: String
    
    init? (DeltaName: String, DeltaImages: [UIImage]) {
        self.DeltaName = DeltaName
        self.DeltaImages = DeltaImages
        
        if DeltaName.isEmpty {
            return nil
        }
        
    }
 
    
}
