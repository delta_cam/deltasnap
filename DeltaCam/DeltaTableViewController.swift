//
//  DeltaTableViewController.swift
//  DeltaCam
//
//  Created by Mars Huang on 5/28/16.
//  Copyright © 2016 DeltaCam. All rights reserved.
//

import UIKit
import CoreData

class DeltaTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    //Mark: Properties
    
    let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var Deltas = [Delta]()
    
    var navigationBar:UINavigationBar=UINavigationBar()
    var fetchedResultsController: NSFetchedResultsController!
//    let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let request = NSFetchRequest(entityName: "Delta")
    var commitPredicate: NSPredicate?
    var context: NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationBar.translucent = false
        //setNavBar()
        //CORE DATA FETCH
        
        request.returnsObjectsAsFaults = false
        /*
        let context: NSManagedObjectContext = appDel.managedObjectContext
        do {
            let results = try context.executeFetchRequest(request)
            if results.count > 0 {
                print(results.count)
                for result in results as! [NSManagedObject]{
                    let deltaName = try result.valueForKey("deltaName") as! String
                    print(deltaName)
                    let deltaImage = result.valueForKey("deltaImages") as! [UIImage]
                    let fetchDelta = Delta(DeltaName: deltaName, DeltaImages: deltaImage)!
                    Deltas += [fetchDelta]
                }
            }
        } catch {
            print("Fetch Failed")
        }
        */
        loadSavedData()
        navigationItem.leftBarButtonItem = editButtonItem()
        //loadSampleDeltas()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func loadSavedData() {
        let context: NSManagedObjectContext = appDel.managedObjectContext
        if fetchedResultsController == nil {
            let fetch = NSFetchRequest(entityName: "Delta")
            let sort = NSSortDescriptor(key: "deltaName", ascending: false)
            fetch.sortDescriptors = [sort]
            fetch.fetchBatchSize = 10
            fetchedResultsController = NSFetchedResultsController(fetchRequest: fetch, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            fetchedResultsController.delegate = self
        }
        fetchedResultsController.fetchRequest.predicate = commitPredicate
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
            print("Fetch failed")
        }
    }
    
    //MARK: Style: 
    func setNavBar(){
        navigationBar.frame = (CGRectMake(0, 0, 320, 500))
        navigationBar.backgroundColor = (UIColor.blackColor())
        self.view.addSubview(navigationBar)
    }
    
    
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "DeltaTableVIewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! DeltaTableViewCell
        //let delta = Deltas[indexPath.row]
        let delta = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Delta
        print(delta.deltaName)
        cell.DeltaName.text = delta.deltaName
        //What if they don't have photos in their Delta yet
        cell.DeltaPhoto.image = delta.deltaImages![0] as! UIImage
        // Configure the cell..
        return cell
    }


    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            context = appDel.managedObjectContext
            let commit = fetchedResultsController.objectAtIndexPath(indexPath) as! Delta
            context.deleteObject(commit)
            saveContext()
            /*
            let request = NSFetchRequest(entityName: "Delta")
            request.predicate = NSPredicate(format: "deltaName = %@", Deltas[indexPath.row].deltaName!)
            do {
                let results = try context.executeFetchRequest(request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject]{
                        context.deleteObject(result)
                    }
                }
            }catch {
                print("Fetch Failed")
            }
            Deltas.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
 */
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    
    func saveContext() {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                print("An error occurred while saving: \(error)")
            }
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
            
        default:
            break
        }
    }
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail"{
            let DeltaDetailViewController = segue.destinationViewController as! DeltaViewController
            if let selectedDeltaCell = sender as? DeltaTableViewCell{
                let indexPath = tableView.indexPathForCell(selectedDeltaCell)
                //let selectedDelta = Deltas[indexPath!.row]
                let selectedDelta = fetchedResultsController.objectAtIndexPath(indexPath!) as! Delta
                DeltaDetailViewController.delta = selectedDelta
            }
        }
        else if segue.identifier == "AddItem"{
            print("Adding New Delta")
        }
    }
    
    @IBAction func unwindToDeltaList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.sourceViewController as? DeltaViewController {
            if let selectedIndexPath = tableView.indexPathForSelectedRow{
                //Deltas[selectedIndexPath.row] = delta
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            }
            else{
                loadSavedData()
                //let newIndexPath = NSIndexPath(forRow: fetchedResultsController.sections![0].numberOfObjects, inSection: 0)
                //Deltas.append(delta)
                //tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
            }
        }
    }

}
