//
//  NewDelta.swift
//  DeltaCam
//
//  Created by Mars Huang on 7/30/16.
//  Copyright © 2016 DeltaCam. All rights reserved.
//

import UIKit
import CoreData

class NewDelta: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIGestureRecognizerDelegate, UIPickerViewDelegate{
    
    
    //MARK: Properties
    var delta: Delta?
    
    @IBOutlet weak var DeltaName: UILabel!
    @IBOutlet weak var DeltaNameTextField: UITextField!
    @IBOutlet weak var NextButton: UIBarButtonItem!

    var hasName = false

    //MARK: Navigation
    @IBAction func cancel(sender: UIBarButtonItem) {
      dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func saveButton(sender: UIBarButtonItem) {
        //UserProfile.savePressed = true
        let userProfile = UserProfile()
        userProfile.savePressed = true
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context: NSManagedObjectContext = appDel.managedObjectContext
        let deltaName = DeltaName.text ?? ""
        let coreDelta = NSEntityDescription.insertNewObjectForEntityForName("Delta", inManagedObjectContext: context)
        coreDelta.setValue(deltaName, forKey: "deltaName")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toNewPhoto"{
            let secondViewController = segue.destinationViewController as? New_Photo
            if let svc = secondViewController{
                svc.DeltaName = DeltaNameTextField.text ?? " "
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DeltaNameTextField.delegate = self
        if ((DeltaNameTextField.text?.isEmpty) == true){
            NextButton.enabled=false
        }
        DeltaNameTextField.autocapitalizationType = UITextAutocapitalizationType.Words
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func dismissKeyboard(){
        view.endEditing(true)
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        print("im here")
        //textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        //*******Send delta name to next page
        //DeltaName.text = DeltaNameTextField.text!
        //checkValidDeltaName()
        
        //navigationItem.title = DeltaNameTextField.text
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        //saveButton.enabled = false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let text = (DeltaNameTextField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
        if !text.isEmpty{
            NextButton.enabled = true
        } else {
            NextButton.enabled = false
        } 
        return true
    }
    
/*
    func checkValidDeltaName() {
        let text = DeltaNameTextField.text ?? ""
        if !text.isEmpty{
            hasName = true
        }
        if hasName{
            self.navigationItem.rightBarButtonItem?.enabled = false
        }
    }
*/
 }

