//
//  DeltaViewController.swift
//  DeltaCam
//
//  Created by Mars Huang on 5/19/16.
//  Copyright © 2016 DeltaCam. All rights reserved.
//

import UIKit
import CoreData

class DeltaViewController: UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIGestureRecognizerDelegate,UIPopoverPresentationControllerDelegate {
    
    //MARK: Properties
    var delta: Delta?
    @IBOutlet weak var DeltaImage: UIImageView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var forwardButton: UIView!
    @IBOutlet weak var playButton: UIView!
    @IBOutlet weak var backButton: UIView!
    
    var timer = NSTimer()
    var isAnimating = false
    var counter = 0
    var newPhoto = false
    var hasName = false
    var playing = false
    
//ViewDidLoad

    override func viewWillAppear(animated: Bool) {
        print("herrow")
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillAppear(animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let delta = delta{
            navigationItem.title = delta.deltaName!
            DeltaImage.image = delta.deltaImages![0] as! UIImage
            
        }
        self.view.userInteractionEnabled = true
        saveButton.enabled = false
        playing = false
        view.sendSubviewToBack(DeltaImage)
        /*
         if !newPhoto {
         print(newPhoto)
         saveButton.enabled = false
         }
         */
        //checkValidDeltaName()
        
        //MARK: Tap Gestures
        let tapBack = UITapGestureRecognizer(target: self, action: #selector(DeltaViewController.PrevDeltaButton(_:)))
        tapBack.delegate = self
        tapBack.numberOfTapsRequired = 1
        backButton.addGestureRecognizer(tapBack)
        backButton.backgroundColor = UIColor.clearColor()
        
        let tapPlay = UITapGestureRecognizer(target: self, action: #selector(DeltaViewController.PlayButton(_:)))
        tapPlay.delegate = self
        tapPlay.numberOfTapsRequired = 1
        playButton.addGestureRecognizer(tapPlay)
        playButton.backgroundColor = UIColor.clearColor()

        
        let tapNext = UITapGestureRecognizer(target: self, action: #selector(DeltaViewController.NextDeltaButton(_:)))
        tapNext.delegate = self
        tapNext.numberOfTapsRequired = 1
        forwardButton.addGestureRecognizer(tapNext)
        forwardButton.backgroundColor = UIColor.clearColor()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Navigation
    @IBAction func cancel(sender: UIBarButtonItem) {
        //dismissViewControllerAnimated(true, completion: nil)
        let isPresentingInAddDeltaMode = presentingViewController is UINavigationController
        timer.invalidate()
        if isPresentingInAddDeltaMode{
            dismissViewControllerAnimated(true, completion: nil)
        }
        else{
            navigationController!.popViewControllerAnimated(true)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context: NSManagedObjectContext = appDel.managedObjectContext
        if saveButton === sender {
            //let deltaName = DeltaNameTextField.text ?? ""
            let photo = DeltaImage.image
            let isPresentingInAddDeltaMode = presentingViewController is UINavigationController
            if isPresentingInAddDeltaMode{
/*
                print("going back")
                let coreDelta = NSEntityDescription.insertNewObjectForEntityForName("Delta", inManagedObjectContext: context)
                coreDelta.setValue(deltaName, forKey: "deltaName")
                //let nsImage :NSData = UIImagePNGRepresentation(photo!)!
                let photoArray:[UIImage] = [photo!]
                let corePhotoArray: NSMutableArray = [photo!]
                coreDelta.setValue(corePhotoArray, forKey: "deltaImages")
                //delta = Delta(deltaName: deltaName, deltaImages: photoArray)
                delta?.deltaName = deltaName
                delta?.deltaImages = photoArray as! NSMutableArray
*/
 }
            else{
                let request = NSFetchRequest(entityName: "Delta")
                request.predicate = NSPredicate(format: "deltaName = %@", (delta?.deltaName)!)
                
                //delta?.deltaImages!.addObject(photo!)
                
                //var corePhotoArray: NSMutableArray = []
                //for i in (delta?.DeltaImages)!{
                //    corePhotoArray.addObject(i)
                //}
                let privateContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
                privateContext.persistentStoreCoordinator = context.persistentStoreCoordinator
                privateContext.performBlock {
                    // Code in here is now running "in the background" and can safely
                    // do anything in privateContext.
                    // This is where you will create your entities and save them.
                    do {
                        let results = try context.executeFetchRequest(request)
                        if results.count > 0 {
                            print(results.count)
                            for result in results as! [NSManagedObject]{
                                var corePhotoArray = result.valueForKey("deltaImages")
                            //*******commenting this out only increase count by 1, buyt not saving acual photo (Below)
                            //corePhotoArray!.addObject(photo!)
//                            result.setValue(deltaName, forKey: "deltaName")
                                result.setValue(corePhotoArray, forKey: "deltaImages")
                                print("going back to main")
                            }
                        }
                    }catch{
                        print("error")
                    }
                    do {
                        try context.save()
                    }catch{
                        print("can't save")
                    }
                }
            }
        }
    }
    
            
            /*
            let request = NSFetchRequest(entityName: "Delta")
            request.predicate = NSPredicate(format: "deltaName = %@", "delta1")      //%@ means string placeholder
            request.returnsObjectsAsFaults = false
            do {
                let results = try context.executeFetchRequest(request)
                if results.count > 0 {
                    for result in results as! [NSManagedObject]{
                        
                        
                        //changing value in database
                        result.setValue("Paenas", forKey: "deltaName")
                        do {
                            try context.save()
                        } catch {}
                        
                        //deleting stuff from database
                        context.deletedObjects("result")
                        
                        
                        if let deltaName = result.valueForKey("deltaName") as? String {
                            print(result.valueForKey("deltaName"))
                        }
                        if let deltaImage = result.valueForKey("deltaImages") as? [UIImage] {
                            print(result.valueForKey("deltaImages"))
                        }
                    }
                }
            } catch {
                print("Fetch Failed")
            }
            
            
            */

    //MARK: UITextFieldDelegate
/*
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        checkValidDeltaName()
        navigationItem.title = textField.text
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        saveButton.enabled = false
    }
    
    func checkValidPhoto(){
        if delta?.DeltaImages.count == numPhoto[1]{
            saveButton.enabled = false
        }
    }

    func checkValidDeltaName() {
        if !text.isEmpty{
            hasName = true
        }
        if hasName && newPhoto {3
            saveButton.enabled = true
        }
    }
*/
    
//MARK: menuViewController
    @IBAction func menuView(sender: UIBarButtonItem) {
        print("tapped")
        let storyboard : UIStoryboard = UIStoryboard(
            name: "Main",
            bundle: nil)
        var menuViewController: UIViewController = storyboard.instantiateViewControllerWithIdentifier("menuViewController") as UIViewController
        menuViewController.modalPresentationStyle = .Popover
        let width = UIScreen.mainScreen().bounds.size.width
        menuViewController.preferredContentSize = CGSizeMake(width, 100)
        let popoverMenuViewController = menuViewController.popoverPresentationController
        popoverMenuViewController?.barButtonItem = sender
        popoverMenuViewController?.permittedArrowDirections = .Any
        popoverMenuViewController?.delegate = self
        popoverMenuViewController?.sourceView = self.view

       /* popoverMenuViewController?.sourceRect = CGRect(
            //x: location.x,
            //y: location.y,
            //width: 1,
            //height: 1)
        */
        print("before")
        presentViewController(menuViewController, animated: true, completion: nil)
        print("after")
    }
    
    func adaptivePresentationStyleForPresentationController(
        controller: UIPresentationController!) -> UIModalPresentationStyle {
        return .None
    }
    
// MARK: DeletePHoto
    
    @IBAction func deleteButton(sender: UIBarButtonItem) {
        print("Herdur")
        let size:Int! = delta?.deltaImages!.count
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context: NSManagedObjectContext = appDel.managedObjectContext
        let request = NSFetchRequest(entityName: "Delta")
        request.predicate = NSPredicate(format: "deltaName = %@", (self.delta?.deltaName)!)
        if size == 1 {
            let alertController = UIAlertController(title: "DeletePhoto", message: "This is the last photo in your Delta. Deleting this photo will result in deleting your Delta, are you sure you want to continue?", preferredStyle: UIAlertControllerStyle.ActionSheet)
            let Delete = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Destructive) { Void in
                do {
                    let fetchedEntities = try context.executeFetchRequest(request) as! [Delta]
                    if let entityToDelete = fetchedEntities.first {
                        context.deleteObject(entityToDelete)
                    }
                }catch{
                    print("error")
                }
                do {
                    try context.save()
                }catch{
                    print("can't save")
                }
                let userProfile = UserProfile()
                userProfile.savePressed = true
                self.navigationController?.popViewControllerAnimated(true)
            }
            alertController.addAction(Delete)
            let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { Void in
            }
            alertController.addAction(cancelButton)
            presentViewController(alertController, animated: true, completion: nil)
        }
        else{
            let alertController = UIAlertController(title: "DeletePhoto", message: "Are you sure you want to delete this photo?", preferredStyle: UIAlertControllerStyle.ActionSheet)
            let Delete = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Destructive) { Void in
                self.delta?.deltaImages!.removeObjectAtIndex(self.counter)
                let privateContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
                privateContext.persistentStoreCoordinator = context.persistentStoreCoordinator
                privateContext.performBlock {
                    do {
                        let results = try context.executeFetchRequest(request)
                        if results.count > 0 {
                            for result in results as! [NSManagedObject]{
                                var corePhotoArray = result.valueForKey("deltaImages")
                                result.setValue(corePhotoArray, forKey: "deltaImages")
                            }
                        }
                    }catch{
                        print("error")
                    }
                    do {
                        try context.save()
                    }catch{
                        print("can't save")
                    }
                }
                if self.counter == (0){
                    self.counter = size-1
                }
                self.counter = self.counter-1
                self.DeltaImage.image = self.delta!.deltaImages![self.counter] as! UIImage
            }
            alertController.addAction(Delete)
            let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { Void in
            }
            alertController.addAction(cancelButton)
            presentViewController(alertController, animated: true, completion: nil)
        }
    }
   
    
 //MARK: UIImagePICkerCOntrollerDelegate
    
    @IBAction func CameraButton(sender: UIBarButtonItem) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .Camera
        imagePickerController.delegate = self
        
        if delta?.deltaImages!.count > 0{
            let backgroundImage = delta?.deltaImages![(delta?.deltaImages!.count)!-1] as! UIImage
            let width = UIScreen.mainScreen().bounds.size.width
            let height = (width/3)*4
            print(UIScreen.mainScreen().bounds.size.height)
            let heightOffset = (UIScreen.mainScreen().bounds.size.height)/(667/44.5)
            let backgroundImageView = UIImageView(frame: CGRectMake(0, heightOffset, width, height))
            backgroundImageView.image = backgroundImage
            //let backgroundImageView = UIImageView(image: backgroundImage)
            backgroundImageView.alpha = 0.3
            //backgroundImageView.contentMode = .ScaleAspectFit
            imagePickerController.cameraOverlayView = backgroundImageView
            //imagePickerController.allowsEditing = true
            let center = NSNotificationCenter.defaultCenter()
            center.addObserverForName("_UIImagePickerControllerUserDidCaptureItem", object: nil, queue: nil, usingBlock:{_ in
                print("helo")
                backgroundImageView.alpha = 0.0
            })
        }
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        newPhoto = true
        DeltaImage.image = image
        self.dismissViewControllerAnimated(true, completion: nil)
        DeltaImage.contentMode = .ScaleAspectFit
        saveButton.enabled = true
        delta?.deltaImages!.addObject(image)
        let size:Int! = delta?.deltaImages!.count
        counter == size-1
    }
    
    
//MARK: Actions
    
    func doAnimation(){
        let size:Int! = delta?.deltaImages!.count
        //print(size)
        if counter == (size-1){
            counter = 0
        }
        else{
            counter += 1
        }
        
        let toImage = delta?.deltaImages![counter] as! UIImage
        UIView.transitionWithView(self.DeltaImage, duration: 1, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {self.DeltaImage.image = toImage}, completion: nil)
        
        
        //DeltaImage.image = delta?.DeltaImages[counter]
        //print(counter)
    }

    
    func PlayButton(sender: UITapGestureRecognizer? = nil){
        print("play")
        //var toggleBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Play, target: self, action: #selector(DeltaViewController.PlayButton(_:)))
        if isAnimating == true{
            timer.invalidate()
            isAnimating = false
        }
        else {
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(DeltaViewController.doAnimation), userInfo: nil, repeats: true)
            //toggleBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Pause, target: self, action: #selector(DeltaViewController.PlayButton(_:)))
            isAnimating = true
        }
        
        //var items = toolBar.items!
        //items[2] = toggleBtn
        //toolBar.setItems(items, animated: true)
    }
    
    
    func NextDeltaButton(sender: UITapGestureRecognizer) {
        print("next")
        let size:Int! = delta?.deltaImages!.count
        print(size)
        if counter == size-1{
            counter = 0
        }
        else{
            counter = counter+1
        }
        let toImage = delta?.deltaImages![counter] as! UIImage
        UIView.transitionWithView(self.DeltaImage, duration: 1, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {self.DeltaImage.image = toImage}, completion: nil)
        //DeltaImage.image = delta?.DeltaImages[counter]
    }
    
    func PrevDeltaButton(sender: UITapGestureRecognizer) {
        print("back")
        let size:Int! = delta?.deltaImages!.count
        if counter == (0){
            counter = size-1
        }
        else{
            counter = counter-1
        }
        
        let toImage = delta?.deltaImages![counter] as! UIImage
        UIView.transitionWithView(self.DeltaImage, duration: 1, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {self.DeltaImage.image = toImage}, completion: nil)
        //DeltaImage.image = delta?.DeltaImages[counter]
    }
    
//Save to disk 
    /*
    func getDocumentsURL() -> NSURL {
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(filename: String) -> String {
        let fileURL = getDocumentsURL().URLByAppendingPathComponent(filename)
        return fileURL.path!
    }
     
    let myImageName = "image.png"
    let imagePath = fileInDocumentsDirectory(myImageName)
    
    if let image = imageView.image {
        saveImage(image, path: imagePath)
    } else { print("some error message") }
    
    if let loadedImage = loadImageFromPath(imagePath) {
        print(" Loaded Image: \(loadedImage)")
    } else { print("some error message 2") }

    //
     func saveImage (image: UIImage, path: String ) -> Bool{
        let pngImageData = UIImagePNGRepresentation(image)
        //let jpgImageData = UIImageJPEGRepresentation(image, 1.0)   // if you want to save as JPEG
        let result = pngImageData!.writeToFile(path, atomically: true)
        return result
    }
    
    func loadImageFromPath(path: String) -> UIImage? {
        let image = UIImage(contentsOfFile: path)
        if image == nil {
            print("missing image at: \(path)")
        }
        print("Loading image from path: \(path)") // this is just for you to see the path in case you want to go to the directory, using Finder.
        return image
    }
 */
 }

