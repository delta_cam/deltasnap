
import UIKit

class New_Photo: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK: Properties
    var delta: Delta?
    
    @IBOutlet weak var DeltaImage: UIImageView!
    
    var DeltaName:String?
    
    
    var newPhoto = false
    var hasName = false
    
    @IBAction func Back(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toNewReminder"{
            let secondViewController = segue.destinationViewController as? NewReminder
            if let svc = secondViewController{
                svc.DeltaName = DeltaName
                svc.DeltaImage = DeltaImage.image
            }
        }
    }
    
    
    override func viewDidLoad() {
        print(DeltaName)
        if (self.DeltaImage.image == nil){
            self.navigationItem.rightBarButtonItem?.enabled = false
        } else {
            self.navigationItem.rightBarButtonItem?.enabled = true
        }
    }
    
    //MARK: UIImagePICkerCOntrollerDelegate
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        newPhoto = true
        self.navigationItem.rightBarButtonItem?.enabled = true
        self.dismissViewControllerAnimated(true, completion: nil)
        DeltaImage.contentMode = .ScaleAspectFit
        DeltaImage.image = image
    }
    
    @IBAction func CameraButton(sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .Camera
        imagePickerController.delegate = self
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
    @IBAction func PhotoButton(sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .PhotoLibrary
        imagePickerController.delegate = self
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    
}

