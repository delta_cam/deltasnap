//
//  New_Photo.swift
//  DeltaCam
//
//  Created by Mars Huang on 1/19/17.
//  Copyright © 2017 DeltaCam. All rights reserved.
//

import UIKit
import CoreData

class NewReminder:  UIViewController, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIGestureRecognizerDelegate, UIPickerViewDelegate,UIPickerViewDataSource{
    //MARK: Properties
    var delta: Delta?
    var DeltaName:String?
    var DeltaImage:UIImage?
    
    
    @IBOutlet weak var reminderSwitch: UISwitch!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var startFrom: UILabel!
    @IBOutlet weak var repeatLabel: UILabel!
    @IBOutlet weak var repeatNumber: UITextField!
    var interval:String = ""
    var numRepeats = 0
    var reminderFlag = false
    var pickerData: [String] = [String]()
    //MARK: Navigation
    
    @IBAction func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    /*
     @IBAction func saveButton(sender: UIBarButtonItem){
     performSegueWithIdentifier("saveButton", sender: UIBarButtonItem.self)
     }
     */
    
    @IBAction func saveButton(sender: UIBarButtonItem) {
        let userProfile = UserProfile()
        userProfile.savePressed = true
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context: NSManagedObjectContext = appDel.managedObjectContext
        let deltaName = DeltaName
        let photo = DeltaImage
        let coreDelta = NSEntityDescription.insertNewObjectForEntityForName("Delta", inManagedObjectContext: context)
        coreDelta.setValue(deltaName, forKey: "deltaName")
        let deltaStartDate = NSDate()
        coreDelta.setValue(deltaStartDate, forKey: "deltaStartDate")
        //let nsImage :NSData = UIImagePNGRepresentation(photo!)!
        //let photoArray:[UIImage] = [photo!]
        let corePhotoArray: NSMutableArray = [photo!]
        coreDelta.setValue(corePhotoArray, forKey: "deltaImages")
        //delta = Delta(DeltaName: deltaName, DeltaImages: photoArray)
        dismissViewControllerAnimated(true, completion: nil)
        print(reminderFlag)
        if reminderFlag{
            numRepeats = Int(repeatNumber.text!)!
            print("reminderFlag on")
            let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
            if settings!.types == .None {
                print("returning statement")
                let ac = UIAlertController(title: "Can't schedule", message: "Either we don't have permission to schedule notifications, or we haven't asked yet.", preferredStyle: .Alert)
                ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
                presentViewController(ac, animated: true, completion: nil)
                return
            }
            var notification = UILocalNotification()
            //BASED ON THE DATE/TIME THEY CHOOSE
            
            notification.fireDate = datePicker.date
            var startDate = datePicker.date
            var theCalendar = NSCalendar.currentCalendar()
            //var repeatCountDesired = 1
            /*
             if Int(frequencyTextField.text!.count!) > 0 {
             repeatCountDesired = Int(frequencyTextField.text!)
             }
             */
            switch(interval){
            case "Test":
                print("selected test")
                notification.repeatInterval = NSCalendarUnit.Minute
            case "Day" :
                notification.repeatInterval = NSCalendarUnit.Day
            case "Week":
                notification.repeatInterval = NSCalendarUnit.WeekOfYear
            case "Month":
                notification.repeatInterval = NSCalendarUnit.Month
            default:
                print("nothing chosen")
            }
            
            if numRepeats > 0 && numRepeats <= 64 {
                print("in if statement")
                for var k in 1...numRepeats{
                    notification = UILocalNotification()
                    notification.alertBody = "BRUH! TAKE YO PICS"
                    notification.fireDate = startDate
                    //notification.alertAction = "be awesome!"
                    notification.soundName = UILocalNotificationDefaultSoundName
                    notification.userInfo = ["CustomField1": "w00t"]
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    switch(interval){
                    case "Test":
                        print("setting timer")
                        var minComponent = NSDateComponents()
                        minComponent.minute = 1
                        startDate = theCalendar.dateByAddingComponents(minComponent, toDate: startDate, options: [])!
                        print("done settting timer")
                    case "Day" :
                        var dayComponent = NSDateComponents()
                        dayComponent.day = 1
                        startDate = theCalendar.dateByAddingComponents(dayComponent, toDate: startDate, options: [])!
                    case "Week":
                        var weekComponent = NSDateComponents()
                        weekComponent.weekOfYear = 1
                        startDate = theCalendar.dateByAddingComponents(weekComponent, toDate: startDate, options: [])!
                    case "Month":
                        var monthComponent = NSDateComponents()
                        monthComponent.month = 1
                        startDate = theCalendar.dateByAddingComponents(monthComponent, toDate: startDate, options: [])!
                    default:
                        print("nothing chosen")
                    }
                    
                }
            }
        }
    }
    
    
    //MARK:Switch
    func switchStatus(reminderSwitch: UISwitch){
        print("1")
        if reminderSwitch.on == true{
            print("2")
            pickerView.hidden = false
            datePicker.hidden = false
            startFrom.hidden = false
            repeatLabel.hidden = false
            repeatNumber.hidden = false
            //frequencyTextField.hidden = false
            reminderFlag = true
            let notificationSettings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(notificationSettings)
        }
        else{
            pickerView.hidden = true
            datePicker.hidden = true
            startFrom.hidden = true
            reminderFlag = false
            repeatLabel.hidden = true
            repeatNumber.hidden = true
            //frequencyTextField.hidden=true
        }
        
    }
    
    
    func dismissKeyboard(){
        repeatNumber.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard")))
        
        let currentDate: NSDate = NSDate()
        
        self.datePicker.minimumDate = currentDate
        
        datePicker.minimumDate = NSDate()
        repeatNumber.delegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
        pickerData = ["Test","Day", "3 Days", "Week", "Month", "April 1st"]
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        pickerView.hidden = true
        datePicker.hidden = true
        startFrom.hidden = true
        repeatNumber.hidden = true
        repeatLabel.hidden = true
        
        for notification in UIApplication.sharedApplication().scheduledLocalNotifications as [UILocalNotification]! {
            UIApplication.sharedApplication().cancelLocalNotification(notification)
        }
        reminderSwitch.addTarget(self, action: #selector(self.switchStatus(_:)), forControlEvents: UIControlEvents.ValueChanged)
        print("turning reminderfalg off")
        reminderSwitch.on = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: PickerView
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //return pickerData[component][row]
        interval = pickerData[row]
        return pickerData[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
    }

 }
