//
//  DeltaTableViewController.swift
//  DeltaCam
//
//  Created by Mars Huang on 5/28/16.
//  Copyright © 2016 DeltaCam. All rights reserved.
//

import UIKit
import CoreData

class UserProfile:UIViewController, UITableViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NSFetchedResultsControllerDelegate {
    
    //Mark: Properties
    @IBOutlet weak var tableView: UITableView!
    let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var fetchedResultsController: NSFetchedResultsController!
    let request = NSFetchRequest(entityName: "Delta")
    var commitPredicate: NSPredicate?
    var context: NSManagedObjectContext!
    let gradientLayer = CAGradientLayer()
    @IBOutlet weak var ProfileBackground: UIView!
    @IBOutlet weak var UserPhoto: UIImageView!
    @IBOutlet weak var NewDelta: UIButton!
    @IBOutlet weak var UserName: UITextField!
    var savePressed:Bool = false
    
    //var Deltas = [Delta]()
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let height = UIScreen.mainScreen().bounds.size.height
        let width = UIScreen.mainScreen().bounds.size.width
        var profileFrame:CGRect = self.ProfileBackground.frame
        profileFrame.size.height = height/2
        profileFrame.origin.y = 0.0
        ProfileBackground.frame = profileFrame
        //ProfileBackground.alpha = 0.0
        
        var tableFrame:CGRect = self.tableView.frame
        tableFrame.size.height = height/2
        tableFrame.origin.y = height/2
        tableView.frame = tableFrame
        
        var buttonFrame:CGRect = self.NewDelta.frame
        buttonFrame.origin.y = height/2
        NewDelta.frame = buttonFrame
        
        
        //UserPhoto
        UserPhoto.layer.cornerRadius = UserPhoto.frame.size.height/2
        UserPhoto.layer.masksToBounds = true
        UserPhoto.layer.borderWidth = 5
        let white = UIColor.whiteColor()
        UserPhoto.layer.borderColor = white.CGColor
        
        //NewDelta
        NewDelta.backgroundColor = UIColor.lightGrayColor()
        NewDelta.layer.cornerRadius = NewDelta.frame.size.height/2
        NewDelta.layer.masksToBounds = true
        //NewDelta.layer.borderWidth = 1.5
        //NewDelta.layer.borderColor = UIColor.whiteColor().CGColor
        
        //Gradient
        gradientLayer.frame = self.ProfileBackground.bounds
        let color2 = UIColor.blackColor().CGColor as CGColorRef
        let color1 = UIColor.grayColor().CGColor as CGColorRef
        gradientLayer.colors = [color2, color1]
        gradientLayer.locations = [0.0, 1.0]
        self.ProfileBackground.layer.insertSublayer(gradientLayer, atIndex: 0)
        
        
        //TableView color
        self.tableView.backgroundColor = UIColor.lightGrayColor()
        
        //ProfileBackground:
        ProfileBackground.layer.shadowOpacity = 0.9
        ProfileBackground.layer.shadowRadius = 5.0
        ProfileBackground.layer.shadowOffset = CGSizeMake(0, 2)
        ProfileBackground.layer.shadowColor = UIColor.blackColor().CGColor

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        UserName.attributedPlaceholder = NSAttributedString(string: "Enter Name",attributes: [NSForegroundColorAttributeName: UIColor.whiteColor()])
        print("1")
        print("3")
        //NewDelta
        NewDelta.backgroundColor = UIColor.lightGrayColor()
        NewDelta.layer.cornerRadius = NewDelta.frame.size.height/2
        NewDelta.layer.masksToBounds = true
        //NewDelta.layer.borderWidth = 1.5
        //NewDelta.layer.borderColor = UIColor.whiteColor().CGColor
        
        //CORE DATA FETCH
        context = appDel.managedObjectContext
        loadSavedData()
        
        //NSUserDefaults: 
        UserName.text = NSUserDefaults.standardUserDefaults().objectForKey("userName")as! String!
        UserName.font = UserName.font?.fontWithSize(41)
        UserName.textColor = UIColor.whiteColor()
        if let tempUserPhoto = NSUserDefaults.standardUserDefaults().objectForKey("userPhoto"){
            UserPhoto.image = UIImage(data: (tempUserPhoto as? NSData)!)
        }
        self.UserName.delegate = self
        
        //Tap Gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(UserProfile.editPhoto(_:)))
        tap.delegate = self
        UserPhoto.addGestureRecognizer(tap)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        //navigationItem.leftBarButtonItem = editButtonItem()
        //loadSampleDeltas()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Navigation Controller Hide:
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewWillAppear(animated)
        if savePressed{
            loadSavedData()
            tableView.reloadData()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        super.viewWillDisappear(animated)
    }
    
//MARK: CoreData
    func loadSavedData() {
        let context: NSManagedObjectContext = appDel.managedObjectContext
        if fetchedResultsController == nil {
            let fetch = NSFetchRequest(entityName: "Delta")
            let sort = NSSortDescriptor(key: "deltaStartDate", ascending: false)
            fetch.sortDescriptors = [sort]
            fetch.fetchBatchSize = 10
            fetchedResultsController = NSFetchedResultsController(fetchRequest: fetch, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            fetchedResultsController.delegate = self
        }
        fetchedResultsController.fetchRequest.predicate = commitPredicate
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
            print("Fetch failed")
        }
    }
    
    func saveContext() {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                print("An error occurred while saving: \(error)")
            }
        }
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.grayColor()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        if sectionInfo.numberOfObjects == 0{
            print("2")
            tableView.backgroundColor = UIColor.clearColor()
            tableView.alpha=0
            
            //let noDataLabel: UILabel     = UILabel(frame: CGRectMake(0, 0, tableView.bounds.size.width, tableView.bounds.size.height))
            //noDataLabel.text             = "No data available"
            //noDataLabel.textColor        = UIColor.blackColor()
            //noDataLabel.textAlignment    = .Center
            //tableView.backgroundView = noDataLabel
            //tableView.separatorStyle = .None
        }
        else{
            tableView.backgroundColor = UIColor.grayColor()
            tableView.alpha=1.0
        }
        return sectionInfo.numberOfObjects
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "DeltaTableVIewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! DeltaTableViewCell
        let delta = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Delta
        cell.DeltaName.text = delta.deltaName
        cell.DeltaName.textColor = UIColor.whiteColor()
        
        //******fatal error: unexpectedly found nil while unwrapping an Optional value
        cell.DeltaPhoto.image = delta.deltaImages![0] as! UIImage
        
        //Change cell look;
        cell.layer.masksToBounds = false
        //cell.layer.backgroundColor = UIColor(red: 238, green: 233, blue: 233, alpha: 1.0).CGColor
        cell.layer.shadowOpacity = 1.0
        cell.layer.shadowRadius = 10.0
        cell.layer.shadowOffset = CGSizeMake(0, 0)
        cell.layer.shadowColor = UIColor.blackColor().CGColor
        return cell
    }
    
    // Override to support conditional editing of the table view.
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            context = appDel.managedObjectContext
            let commit = fetchedResultsController.objectAtIndexPath(indexPath) as! Delta
            context.deleteObject(commit)
            saveContext()
            //tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
        }
    }
    

 
//MARK: UserPhoto
    func editPhoto(sender:UITapGestureRecognizer){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        print("here")
        let alertController = UIAlertController(title: "Change Profile Picture", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        let Camera = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default) { Void in
            imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(imagePickerController, animated: true, completion: nil)
        }
        alertController.addAction(Camera)
        let PhotoLibrary = UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.Default) { Void in
            imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            self.presentViewController(imagePickerController, animated: true, completion: nil)
            
        }
        alertController.addAction(PhotoLibrary)
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { Void in
        }
        alertController.addAction(cancelButton)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        UserPhoto.image = image
        self.dismissViewControllerAnimated(true, completion: nil)
        UserPhoto.contentMode = .ScaleAspectFill
        let tempUserPhoto: NSData = UIImageJPEGRepresentation(UserPhoto.image!, 1.0)!
        NSUserDefaults.standardUserDefaults().setObject(tempUserPhoto, forKey: "userPhoto")
    }
    
//MARK: - TextField:
    func textFieldDidEndEditing(textField: UITextField) {
        NSUserDefaults.standardUserDefaults().setObject(UserName.text as String!, forKey: "userName")
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        self.view.endEditing(true)
        //textField.resignFirstResponder()
        return false
    }

    
    
    
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowDetail"{
            let DeltaDetailViewController = segue.destinationViewController as! DeltaViewController
            //self.navigationController!.pushViewController(DeltaDetailViewController, animated: true)
            if let selectedDeltaCell = sender as? DeltaTableViewCell{
                let indexPath = tableView.indexPathForCell(selectedDeltaCell)
                let selectedDelta = fetchedResultsController.objectAtIndexPath(indexPath!) as! Delta
                DeltaDetailViewController.delta = selectedDelta
            }
        }
        else if segue.identifier == "AddItem"{
            print("Adding New Delta")
        }
    }
    
    @IBAction func unwindToDeltaList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.sourceViewController as? DeltaViewController{
            if let selectedIndexPath = tableView.indexPathForSelectedRow{
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            }
            else{
                loadSavedData()
            }
        }
    }
    
    
/*
    func loadProfile() -> (photo: UIImage,name: String)? {
        return NSKeyedUnarchiver.unarchiveObjectWithFile(self.ArchiveURL.path!) as? (UIImage, String)
    }
    
    func saveProfile() {
        NSKeyedArchiver.archiveRootObject(<#T##rootObject: AnyObject##AnyObject#>, toFile: <#T##String#>)
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(userProfile as! AnyObject, toFile: self.ArchiveURL.path!)
        if !isSuccessfulSave {
            print("Failed to save profile...")
        }
    }
    
    let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("Profile")
    
    func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(name, forKey: PropertyKey.nameKey)
    aCoder.encodeObject(photo, forKey: PropertyKey.photoKey)
    aCoder.encodeInteger(rating, forKey: PropertyKey.ratingKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObjectForKey(PropertyKey.nameKey) as! String
        
        // Because photo is an optional property of Meal, use conditional cast.
        let photo = aDecoder.decodeObjectForKey(PropertyKey.photoKey) as? UIImage
        
        let rating = aDecoder.decodeIntegerForKey(PropertyKey.ratingKey)
        
        // Must call designated initializer.
        self.init(name: name, photo: photo, rating: rating)
    }

*/
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController!, didChangeSection sectionInfo: NSFetchedResultsSectionInfo!, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controller(controller: NSFetchedResultsController!, didChangeObject anObject: AnyObject!, atIndexPath indexPath: NSIndexPath!, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath!) {
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Fade)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        case .Update:
            return
            //Should also manage this case!!!
        //self.configureCell(tableView.cellForRowAtIndexPath(indexPath), atIndexPath: indexPath)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController!) {
        tableView.endUpdates()
    }
}
