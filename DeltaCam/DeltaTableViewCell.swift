//
//  DeltaTableViewCell.swift
//  DeltaCam
//
//  Created by Mars Huang on 5/28/16.
//  Copyright © 2016 DeltaCam. All rights reserved.
//

import UIKit

class DeltaTableViewCell: UITableViewCell {

    //MARK: Properties
    //Old
    //@IBOutlet weak var DeltaPhoto: UIImageView!
    //@IBOutlet weak var DeltaName: UILabel!
    
    //New
    @IBOutlet weak var DeltaPhoto: UIImageView!
    @IBOutlet weak var DeltaName: UILabel!
    
    
    
    /*
    override var frame: CGRect{
        get{
            return super.frame
        }
        set(newFrame){
            var frame = newFrame
            frame.origin.y += 20
            super.frame = frame
        }
    }
    */
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
