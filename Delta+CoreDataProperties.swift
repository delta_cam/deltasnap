//
//  Delta+CoreDataProperties.swift
//  DeltaCam
//
//  Created by Mars Huang on 7/20/16.
//  Copyright © 2016 DeltaCam. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Delta {
    //@NSManaged var DeltaImages: [UIImage]
    //@NSManaged var DeltaName: String
    @NSManaged var deltaImages: NSMutableArray?
    @NSManaged var deltaName: String?

}
